import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;


public class flightradar {

    public static void main(String[] args) throws IOException {
        long time;
        pokazInformacje();

        while (1 == 1) {
            time = System.currentTimeMillis();
            while ((System.currentTimeMillis() - time)/1000 < 30) {
                if ((System.currentTimeMillis() - time)/1000 >= 30) {
                    pokazInformacje();
                }
            }
        }
    }

    private static void pokazInformacje() throws IOException {
        String json = getResultAmount("test");
        int planeCount = 0;
        String[] str = new String[json.length()];

        for (int i = 0; i < json.length(); i ++) {
            str[i] = json.substring(i, i + 1);
        }

        for (int i = 0; i < str.length; i++) {
            if (str[i].equals("[")) {
                planeCount++;
            }
        }

        String[] latitude = new String[planeCount];
        String[] longtitude = new String[planeCount];
        String[] planeName = new String[planeCount];

        for (int i = 0; i < planeCount; i++) {
            latitude[i] = "";
            longtitude[i] = "";
            planeName[i] = "";
        }

        planeCount = 0;
        int j = 0;

        for (int i = 30; i < str.length; i++) {
            if (str[i].equals("[")) {
                planeCount++;
                j = 0;
            }
            if (str[i].equals(",") & planeCount > 0) {
                j++;
            } else if (j == 1) {
                latitude[planeCount - 1] += str[i];
            } else if (j == 2) {
                longtitude[planeCount - 1] += str[i];
            } else if (j == 16) {
                planeName[planeCount - 1] += str[i];
            }
        }



        double[] latitudeNumber = new double[planeCount];
        double[] longtitudeNumber = new double[planeCount];
        int[] distance = new int[planeCount];

        for (int i = 0; i < planeCount; i++) {
            latitudeNumber[i] = convert(latitude[i]);
            longtitudeNumber[i] = convert(longtitude[i]);
            distance[i] = (int) distance(latitudeNumber[i], 52.199543080778, longtitudeNumber[i], 21.445520360656104);
            planeName[i] = planeName[i].substring(1, planeName[i].length() - 1);
        }

        System.out.println("Liczba samolot�w: " + planeCount);

        for (int i = 0; i < planeCount; i++) {
            System.out.println(planeName[i] + " | " + distance[i] + "km");
        }
    }

    private static String getResultAmount(String query) throws IOException {
        URLConnection connection = new URL("https://data-live.flightradar24.com/zones/fcgi/feed.js?faa=1&bounds=52.448%2C51.938%2C17.561%2C20.93&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&selected=2903a51c&ems=1&stats=1&fbclid=IwAR1KGuPTCgEROBXdL8oqfsVPGTeuQHYFb4q6uIhaBOmFgLAD4_TvtPkfaj4" + query).openConnection();
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        connection.connect();

        BufferedReader r  = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));

        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            sb.append(line);
        }

        return sb.toString();

    }



    private static double convert(String stringNumber) {
        double doubleNumber = 0;
        int przecinek = 0;

        for (int i = 0; i < stringNumber.length(); i++) {
            switch (stringNumber.substring(stringNumber.length() - i - 1, stringNumber.length() - i)) {
                case "1":
                    doubleNumber += 1 * (Math.pow(10, (i)));
                    break;
                case "2":
                    doubleNumber += 2 * (Math.pow(10, (i)));
                    break;
                case "3":
                    doubleNumber += 3 * (Math.pow(10, (i)));
                    break;
                case "4":
                    doubleNumber += 4 * (Math.pow(10, (i)));
                    break;
                case "5":
                    doubleNumber += 5 * (Math.pow(10, (i)));
                    break;
                case "6":
                    doubleNumber += 6 * (Math.pow(10, (i)));
                    break;
                case "7":
                    doubleNumber += 7 * (Math.pow(10, (i)));
                    break;
                case "8":
                    doubleNumber += 8 * (Math.pow(10, (i)));
                    break;
                case "9":
                    doubleNumber += 9 * (Math.pow(10, (i)));
                    break;
                case ".":
                    przecinek = i;
                    i--;
                    stringNumber = stringNumber.replace(".", "");
                    break;
            }
        }

        doubleNumber /= Math.pow(10, przecinek);

        return doubleNumber;

    }

    public static double distance(double lat1,
                                  double lat2, double lon1,
                                  double lon2)
    {

        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));

        // Radius of earth in kilometers. Use 3956
        // for miles
        double r = 6371;

        // calculate the result
        return(c * r);
    }

}